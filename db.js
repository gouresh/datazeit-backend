const socket = require('./socket')
const Pool = require('pg').Pool
const pool = new Pool({
  user: 'gouresh',
  host: '135.181.102.185',
  database: 'gouresh',
  password: 'testapp',
  port: 5432,
})
const getJobs = (request, response) => {
    console.log(request.query)
    if(request.query.status && request.query.status != ''){
        pool.query('SELECT * FROM queue WHERE status = $1', [request.query.status], (error, results) => {
            if (error) {
              throw error
            }
            response.status(200).json(results.rows)
          })
    }
    else{
        pool.query('SELECT * FROM queue', (error, results) => {
            if (error) {
              throw error
            }
            response.status(200).json(results.rows)
          })
    }
    
  }

const createJob = (request, response, ) => {
    const {job_name} = request.body
    console.log('job name',job_name);
    if(!job_name || job_name == ''){
        
            return response.status(400).send('Job name is mandatory')
          
    }
    pool.query('INSERT INTO queue (job_name) VALUES ($1) RETURNING job_id', [job_name], (error, results) => {
      if (error) {
        throw error
      }
      
      response.status(201).send(`Job added with ID: ${results.rows[0].job_id}`)
      sendJobCount();
    })
  }

const updateJob = (request, response) => {
    
    const { status, job_id } = request.body
    const statuses = ['completed', 'failed']
    if(!statuses.includes(status)){
        response.status(400).send(`Bad Request : Only completed and failed status can be updated`) ;
        return false;
    }
    pool.query(
      'UPDATE queue SET status = $1 WHERE job_id = $2',
      [status, job_id],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).send(`Job modified with ID: ${job_id}`)
        sendJobCount();
      }
    )
  }

  const getJobCount = (request, response) => {
    pool.query(
      'SELECT COUNT(*),status from queue GROUP BY status',
      (error, results) => {
        if (error) {
          throw error
        }
        console.log(results);
        const a =results.rows.reduce((total, obj) => parseInt(obj.count) + total,0)
        console.log(a);
        results.rows.map(r=>{
            r.percent = Math.round((r.count/a)*100);console.log(r.percent)});
        response.status(200).json(results.rows);
      }
    )
  }

  const sendJobCount = () => {
    pool.query(
      'SELECT COUNT(*),status from queue GROUP BY status',
      (error, results) => {
        if (error) {
          throw error
        }
        console.log(results);
        const a =results.rows.reduce((total, obj) => parseInt(obj.count) + total,0)
        console.log(a);
        results.rows.map(r=>{
            r.percent = Math.round((r.count/a)*100);console.log(r.percent)});
        socket.io.sockets.in('queue-data').emit('message', results.rows);
        //response.status(200).json(results.rows);
      }
    )
  }

  module.exports = {getJobs,createJob,updateJob,getJobCount}
  