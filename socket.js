const http = require('http');
const socketserver = require('socket.io');
const instance = http.createServer();
const io = socketserver(instance, {
    cors: {
        origin: '*',
        methods: ["GET", "POST"]
    }
});

io.on('connection', function (socket) {
    console.log('connected');
    socket.on('room', function (room) {
        socket.join(room);
        console.log(room);
        
    });
});

module.exports = {instance,io}