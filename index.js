
const express = require('express')
const bodyParser = require('body-parser')

const cors = require('cors');
const auth = require('./auth.js');
const jobs = require('./jobs.js');
const app = express()
// const redis = require('redis');

// const client = redis.createClient('redis://135.181.102.185:6379');

// client.on('error', (err) => {
//     console.log("Error " + err);
//   });

//   client.on('connect', (err) => {
//     console.log("redis connected");
//   });

app.use(cors());
app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);



app.use('/jobs',auth.verifyKey,jobs.router);

module.exports = {app};