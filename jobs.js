const express = require('express');
const router = express.Router();
const db = require('./db.js')
router.route('/')
                .get(db.getJobs)
                .post(db.createJob)
                .patch(db.updateJob);
router.route('/count')
                .get(db.getJobCount);

module.exports = {router}